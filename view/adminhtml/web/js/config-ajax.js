require([
    "jquery"
], function ($) {
    var searchInputId = "config_search_input";
    var configTableId = "local_config_table";

    init_func();

    var configTr = [
        "id",
        "path",
        "value",
        "scope_id",
        "scope",
    ];

    function init_func() {
        console.log('app/magento/app/code/Debug/Debug/view/adminhtml/web/js/config-ajax.js file loaded');

        $("#config_search_input").keyup(function () {
            searchEvent();
        });
    }

    /**
     * Hide all <tr> of config table which not matches search criteria
     */
    function searchEvent() {
        var searchVal = $('#' + searchInputId).val();

        if (searchVal.length) {
            $('#' + configTableId).find('tr').hide();

            $.each(configTr, function (key, value) {
                $('tr[' + value + '*="' + searchVal + '"]').show();
            });
        } else {
            $('#' + configTableId).find('tr').show();
        }
    }

    function editConfigValueAjax(configId) {
        var data = getDataFromConfigTable(configId);

        if (typeof data === "object") {
            $.ajax({
                url: editConfigValueUrl,
                showLoader: true,
                data: data,
                type: "POST",
                success: function (result) {
                    if (!result.success) {
                        handleError(result);
                        return;
                    }
                    console.log(result);
                },
                error: function (result) {
                    handleError(result);
                }
            });
        }
    }

    function getDataFromConfigTable(configId) {

        var configTr = $('#' + configId);

        //if TR with provided id not found return false
        if (!configTr.length) {
            return false;
        }

        var data = {
            'id': configId,
            'path': configTr.attr('path'),
            'scope': configTr.attr('scope'),
            'scope_id': configTr.attr('scope_id'),
            'old_value': configTr.attr('value'),
            'new_value': configTr.find('#new_value_' + configId).val(),
        };

        return data;
    }

    function handleError(result) {
        console.log(result);
        alert(result.message);
    }

    //define function in global variable so it is accessible in phtml template
    window.editConfigValueAjax = editConfigValueAjax;
    window.searchEvent = searchEvent;
})