<?php
/**
 * @category    Fwc
 * @package     Fwc_Blog
 * @subpackage  Model Block Helper Controller Setup Ui
 * @author      Krystian Banaś <krystian.banas@fastwhitecat.com>
 * @copyright   Copyright (c) 2018 Fast White Cat S. A.
 * @since       1.0.0
 */

namespace Debug\Debug\Controller\Adminhtml\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class ShowConfig extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        echo '<pre>';
        echo 'File: '. __FILE__ .' Line: '. __LINE__;
        var_dump('sdfsdfdsf');
        die();
//        return $resultPage = $this->resultPageFactory->create();
    }
}