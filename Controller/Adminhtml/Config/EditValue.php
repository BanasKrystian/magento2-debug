<?php

namespace Debug\Debug\Controller\Adminhtml\Config;

use Magento\{
    Backend\App\Action\Context,
//    Framework\View\Result\PageFactory,
    Framework\Controller\Result\JsonFactory
};

use \Debug\Debug\Helper\ConfigHelper;

class EditValue extends \Magento\Backend\App\Action
{
//    protected $resultPageFactory;
    protected $resultJsonFactory;
    protected $helper;

    public function __construct(
        Context $context,
//        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        ConfigHelper $helper
    ) {
        parent::__construct($context);

//        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper            = $helper;
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        try {

            $params = $this->_request->getParams();

            if ($this->getRequest()->isAjax()) {

                $this->helper->editValue($params);

                $test = [
                    'success' => true,
                ];

                $result->setData($test);
            }
        } catch (\Exception $ex) {
            $result->setData([
                'success' => false,
                'message' => $ex->getMessage()
            ]);
        }

        return $result;
    }
}
