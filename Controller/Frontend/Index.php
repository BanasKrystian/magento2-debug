<?php
/**
 * @category    Fwc
 * @package     Fwc_Blog
 * @subpackage  Model Block Helper Controller Setup Ui
 * @author      Krystian Banaś <krystian.banas@fastwhitecat.com>
 * @copyright   Copyright (c) 2018 Fast White Cat S. A.
 * @since       1.0.0
 */

namespace Debug\Debug\Controller\Frontend\Config;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        return  $resultPage = $this->resultPageFactory->create();
    }
}
