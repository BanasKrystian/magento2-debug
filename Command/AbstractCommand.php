<?php
/**
 * @category    Fwc
 * @package     Fwc_Blog
 * @subpackage  Model Block Helper Controller Setup Ui
 * @author      Krystian Banaś <krystian.banas@fastwhitecat.com>
 * @copyright   Copyright (c) 2018 Fast White Cat S. A.
 * @since       1.0.0
 */

namespace Debug\Debug\Command;

use Symfony\Component\Console\{
    Command\Command,
    Input\InputInterface,
    Output\OutputInterface,
    Input\InputOption
};

class AbstractCommand extends Command
{

    protected $input;
    private $output;

    protected function init(InputInterface $input, OutputInterface $output)
    {
        $this->input  = &$input;
        $this->output = &$output;
    }
}