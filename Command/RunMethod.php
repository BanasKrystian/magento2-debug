<?php
/**
 * @category    Fwc
 * @package     Fwc_Blog
 * @subpackage  Model Block Helper Controller Setup Ui
 * @author      Krystian Banaś <krystian.banas@fastwhitecat.com>
 * @copyright   Copyright (c) 2018 Fast White Cat S. A.
 * @since       1.0.0
 */

namespace Debug\Debug\Command;

use Symfony\Component\Console\{
    Command\Command,
    Input\InputInterface,
    Output\OutputInterface,
    Input\InputOption
};

class RunMethod extends Command
{

    const CLASS_NAMESPACE = 'class_namespace';
    const METHOD_NAME = 'method_name';

    public function __construct()
    {
        parent::__construct();

    }

    protected function configure()
    {
        $options = [
            new InputOption(
                self::CLASS_NAMESPACE,
                'c',
                InputOption::VALUE_REQUIRED,
                'Classname with namespace'),
            new InputOption(
                self::METHOD_NAME,
                'm',
                InputOption::VALUE_REQUIRED,
                'Method name'),

        ];

        $this->setName("debug:run-method");
        $this->setDescription("Run any method of any class. Warning! There is no way to pass the method parameters, so you'll need to temporary edit method definition.")
            ->setDefinition($options);
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->init($input, $output);
            $output->writeln("Executing class!");


            $output->writeln("Method executed successfully!");
        } catch (\Exception $exception) {
            $output->writeln("There was a problem with execution of method!");
            $output->writeln($exception->getMessage());
        }
    }

    private function init($input, $output)
    {
        $this->input  = &$input;
        $this->output = &$output;

        $this->debug = $this->input->getOption(self::DEBUG_PARAM);
    }
}