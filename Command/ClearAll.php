<?php

namespace Debug\Debug\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClearAll extends Command
{

    public function __construct()
    {
        parent::__construct();

    }

    protected function configure()
    {
        $this->setName("debug:clear-all");
        $this->setDescription("Remove content of var/cache/* var/di/* var/page_cache/* var/generation/*, setup:upgrade. setup:di:compile, cache:flush");
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Start clearing magento!");

        $output->writeln("Remove content of var/cache/* var/di/* var/page_cache/* var/generation/*");
        system('rm -rf var/cache/* var/di/* var/page_cache/* var/generation/* ');

//        $output->writeln("magento setup:upgrade");
//        system('php bin/magento setup:upgrade');
//
//        $output->writeln("magento s:di:comp");
//        system('php bin/magento setup:di:compile');

        $output->writeln("magento cache:flush");
        system('php bin/magento cache:flush');

        $output->writeln("");
        $output->writeln("");
        $output->writeln("Clearing magento finished!");
    }
}