<?php

namespace Debug\Debug\Command;

use Magento\Framework\{
    Module\ModuleList,
    App\ResourceConnection
};
use Symfony\Component\Console\{
    Command\Command,
    Input\InputInterface,
    Output\OutputInterface,
    Input\InputOption
};

class DowngradeModuleVersion extends AbstractCommand
{

    const DEBUG_PARAM = "vvv";
    const TABLE_NAME = 'setup_module';

    protected $input;
    protected $debug;
    protected $output;
    protected $moduleList;
    protected $connection;

    public function __construct(
        ModuleList $moduleList,
        ResourceConnection $connection
    ) {
        parent::__construct();
        $this->moduleList = $moduleList;
        $this->connection = $connection->getConnection();
    }

    protected function configure()
    {
        $options = [
            new InputOption(
                self::DEBUG_PARAM,
                null,
                InputOption::VALUE_NONE,
                'Verbose mode')
        ];

        $this->setName("debug:downgrade-module-versions")
            ->setDescription("Downgrade module versions in case of change of branch or database")
            ->setDefinition($options);

        parent::configure();
    }

    //@todo change debug to false
    private function debug($text, $debug = true)
    {
        if ($this->debug || $debug) {
            $this->output->writeln($text);
        }
    }

    private function debugInLine($text, $debug = true)
    {
        if ($this->debug || $debug) {
            $this->output->write($text);
        }
    }

    protected function init(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
        $this->input  = &$input;
        $this->output = &$output;

        $this->debug = $this->input->getOption(self::DEBUG_PARAM);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $time_start = microtime(true);
        $this->debug("Start setup:upgrade!");
//        system("php bin/magento setup:upgrade");

        $this->debug("Start downgrading module versions!");

        $allModules        = $this->moduleList->getAll();
        $notChangedModules = [];
        $count             = 0;

        if (!count($allModules)) {
            $this->debug("No modules found!");
            return;
        }

        foreach ($allModules as $moduleName => $module) {
            $this->debug(str_pad("Checking " . $moduleName, 50));
            if ($this->downgradeModule($moduleName, $module['setup_version'])) {
                $count++;
            } else {
                $notChangedModules[] = $moduleName;
            }
        }

        $this->debug("");
        $this->debug("Modules: " . implode(", ", $notChangedModules) . " not changed!", false);
        $this->debug("");

        $this->debug("Time: " . (string)((int)(microtime(true) - $time_start)) . "sec");
        $this->debug("Downgraded {$count} of " . count($allModules) . " module(s)!");
        $this->debug("Downgrading module versions finished!");
    }

    private function downgradeModule($moduleName, $version)
    {
        $sql    = $this->connection->select()->from(self::TABLE_NAME)->where('module like ?', $moduleName);
        $result = $this->connection->fetchAll($sql);
        $result = $result[0];

        if (!count($result)) {
            $this->debug("Module not found in " . self::TABLE_NAME . " table!");
            return false;
        }

        $schemaVersion = $result['schema_version'];
        $dataVersion   = $result['data_version'];

        $versionInModuleXml = $this->getModuleVersion($moduleName);
        if (!$versionInModuleXml) {
            $this->debug("Could not find {$moduleName} version in module.xml file!", false);
            return false;
        }
        $this->debug("Xml:" . $versionInModuleXml . " Schema: " . $schemaVersion . " Data: " . $dataVersion, false);

        if ($this->checkModuleVersions($moduleName, $version, $schemaVersion, $dataVersion, $versionInModuleXml)) {
            return $this->changeVersionInDb($moduleName, $versionInModuleXml, $versionInModuleXml);
        }

        return false;
    }

    private function checkModuleVersions(
        $moduleName,
        $magentoModuleVersion,
        $schemaVersion,
        $dataVersion,
        $versionInModuleXml
    ) {

        if ($versionInModuleXml == $schemaVersion && $versionInModuleXml == $dataVersion && $versionInModuleXml == $magentoModuleVersion) {
            $this->debug("{$moduleName} module version is the same as in database!", false);
            return false;
        }

        if (
            version_compare($versionInModuleXml, $schemaVersion) == -1 ||
            version_compare($versionInModuleXml, $dataVersion) == -1
        ) {
            return true;
        }

        return false;
    }

    private function changeVersionInDb($moduleName, $setupVersion, $dataVersion)
    {
        try {
            $bind  = [
                'schema_version' => $setupVersion,
                'data_version'   => $dataVersion
            ];
            $where = [
                'module = ?' => $moduleName
            ];

            $this->connection->update(self::TABLE_NAME, $bind, $where);
            $this->debug("Downgraded {$moduleName} to {$setupVersion}!");
        } catch (\Exception $ex) {
            $this->debug("There was a problem while downgrading {$moduleName}:");
            $this->debug($ex->getMessage());
        }

        return true;
    }

    private function getModuleVersion($moduleName)
    {
        $grepCommand = "grep -rl --include=\"module.xml\" 'name=\"{$moduleName}\"' ";

        //find module.xml file in app/code and vendor paths
        ob_start();
        $vendor = system($grepCommand . 'vendor');
        ob_clean();

        if (strlen($vendor)) {
            return $this->getModuleVersionFromFile($vendor, $moduleName);
        }

        ob_start();
        $appDir = system($grepCommand . 'app/code');
        ob_clean();

        if (strlen($appDir)) {
            return $this->getModuleVersionFromFile($appDir, $moduleName);
        }

        $this->debug("No module.xml found for module {$moduleName}!");
        return false;
    }

    private function getModuleVersionFromFile($appDir, $moduleName)
    {
        if (!is_file($appDir)) {
            $this->debug("Provided path: {$appDir} is not a file!");
            return false;
        }

        $ext = pathinfo($appDir, PATHINFO_EXTENSION);
        if ($ext != 'xml') {
            $this->debug("Provided file: {$appDir} is not a xml file!");
            return false;
        }

        $moduleXml = file_get_contents($appDir);
        $xml       = simplexml_load_string($moduleXml);
        if (!$xml) {
            $this->debug("Provided xml: {$appDir} is not valid!");
            return false;
        }

        //check if xml contain module name the same as we want
        if ($moduleName != $xml->module['name']) {
            $this->debug("Provided xml: {$appDir} is not related to {$moduleName} module!", false);
            return false;
        }

        return $xml->module['setup_version'];
    }

}
