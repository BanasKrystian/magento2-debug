<?php

namespace Debug\Debug\Block;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\App\ResourceConnection;

class Config extends Template
{
    protected $connection;

    public function __construct(
        Context $context,
        ResourceConnection $resource,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->connection = $resource->getConnection(ResourceConnection::DEFAULT_CONNECTION);
    }

    public function getConfigArray()
    {
        $sql    = "SELECT * FROM `core_config_data`";
        $result = $this->connection->fetchAll($sql);

        return $result;
//        return $this->_scopeConfig->getValue('general/region/state_required');
    }

    public function getAjaxUrl(){
        return $this->getUrl("debug/config/editvalue"); // Controller Url
    }

}