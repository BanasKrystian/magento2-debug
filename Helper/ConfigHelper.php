<?php
/**
 * @category    Fwc
 * @package     Fwc_Blog
 * @subpackage  Model Block Helper Controller Setup Ui
 * @author      Krystian Banaś <krystian.banas@fastwhitecat.com>
 * @copyright   Copyright (c) 2018 Fast White Cat S. A.
 * @since       1.0.0
 */

namespace Debug\Debug\Helper;


use Magento\Framework\App\{
    Helper\AbstractHelper,
    Helper\Context,
    Config\Storage\WriterInterface
};
use Magento\Framework\Exception\CouldNotSaveException;

class ConfigHelper extends AbstractHelper
{
    protected $configWriter;

    public function __construct(
        Context $context,
        WriterInterface $configWriter

    ) {
        parent::__construct($context);

        $this->configWriter = $configWriter;
    }

    /**
     * @param $params
     * @throws CouldNotSaveException
     */
    public function editValue($params)
    {
        try {
            $this->configWriter->save(
                $params['path'],
                $params['new_value'],
                $params['scope'],
                $params['scope_id']
            );

            return true;
        } catch (\Exception $exception) {
            $message = "Could not save config value ";
            $message .= $params['path'] ?? "";

            $message .= " " . $exception->getMessage();

            throw new CouldNotSaveException(__($message));
        }
    }
}